---
title: Series of the Week Young Sheldon
date: 2020-04-26
---

Our series actually describes the smallness of Sheldon in the series “The Big Bang Theory”. It begins with Sheldon, who was born in Texas…

* * *

### Series of the Week: Young Sheldon(No Spoiler)

![](https://cdn-images-1.medium.com/max/800/1*qIP0YANeYH5nVNPlCs9Blg.jpeg)

> Our series actually describes the smallness of Sheldon in the series “The Big Bang Theory”. It begins with Sheldon, who was born in Texas in the 80s, and was highly intelligent, started his 9-year-old high school. Due to the poor financial situation of his family, he is enrolled in the public school, where he coaches his father with the best possibilities. Currently in season 3 (2020)

![](https://cdn-images-1.medium.com/max/800/1*-L0cu0TPE4N427na2nz6pw.jpeg)

My opinion

PLUS:

It has a different side and style as a comedy. Especially the players’ good sides in terms of playing the script well and reflecting its time. I can say that the series is also very good in time (18–19 minutes).

NEGATIVE:

The downside of the series is that if you haven’t watched The Big Bang Theory like me or not, you do not fully understand when the series passed. Obviously I had to go to the IMDB page and read. This was the negative side. Now I’m in the middle of the first season.

If you want to have fun and watch the story of this little mischievous, do not stop.

By [Selcuk TATAR](https://medium.com/@tansionline) on [April 26, 2020](https://medium.com/p/57aca04d2b0c).

[Canonical link](https://medium.com/@tansionline/series-of-the-week-young-sheldon-no-spoiler-57aca04d2b0c)

Exported from [Medium](https://medium.com) on January 18, 2022.