---
title: Web Güvenliği Yol Haritası
date: 2021-12-23
---

Selamlar. Ben Selçuk Tatar. Şu an Bilişim Güvenliği bölümü, ilk sınıf öğrencisiyim. Bundan 4 ay önce Web Güvenliğine ilgi duymaya başladım…

* * *

### Web Güvenliği Yol Haritası

Selamlar. Ben Selçuk Tatar. Şu an Bilişim Güvenliği bölümü, ilk sınıf öğrencisiyim. Bundan 4 ay önce Web Güvenliğine ilgi duymaya başladım. Şuan bir amatör olarak size neler öğrendiğimi ve nereden başlayıp şuan nereye doğru ilerlediğimi anlatıyor olucağım. Düzeltmemi istediğiniz birşey olursa [Twitterdan](https://twitter.com/tansionline) bana ulaşabilirsiniz. **Not: İngilizce kaynaklar içerir.**

### Level 0: Hazırlık

İlk önce eyleme geçmeden bazı kavramları bilmeliyiz. Bunun için Zafiyet Araştırmacısı olan Mehmet İncenin [Web Securiy 101](https://www.youtube.com/watch?v=WtHnT73NaaQ&list=PLwP4ObPL5GY940XhCtAykxLxLEOKCu0nT) serisini şiddetle tavsiye ediyorum.

Devam niteliğinde olan [Stanford’un](https://web.stanford.edu/class/cs253/) Computer Science 253 Ders kaynağını önerebilirim.

HTTP anlamak içinse Hacker101'in [Web In Depth](https://www.hacker101.com/sessions/web_in_depth) adlı eğitimini izleyebilirsiniz.

### Level 1: Programlama

![](https://cdn-images-1.medium.com/max/800/0*CR8SfuLRCAD-27lz.png)

Web Güvenliğini anlamak için ilk başta nasıl kodlandığı ve nasıl çalıştığını anlamak gerekir. Bunun içinse ilk başta HTML biçimlendirme dilini bilmek gerekir. HTML dilini öğrenmenin en kolay yollarından biride [W3Schools](https://www.w3schools.com/html/) sitesidir. Buradaki konu akışını anladıktan sonra sizlere verilen etkinlikleri tamamlayabilirsiniz.

Ama HTML ile yapabilecekleriniz sınırlıdır. Javascript ile web sayfalarına resmen can verirsiniz. Javascript öğrenmek içinse sizlere iki kaynak vereceğim. [Learn X in Y minutes](https://learnxinyminutes.com/docs/javascript/) ile Javascript dilinin işlevlerini öğrenebilirsiniz. İkinci önereceğim site ise [Freecodecamp](https://www.freecodecamp.org/learn%20javascript-algorithms-and-data-structures/basic-javascript/) Bu site sizlere yapmanız gerekenleri veriyor ve sizlerde online olarak kodları yazıyorsunuz.

### Level 2: Sanal Makinelerle Antremanlar

![](https://cdn-images-1.medium.com/max/800/0*hnZ5K9T4qSbSBT0I.png)

Programlama ve basit kavramları öğrendiğimize göre artık yavaş yavaş olarak nereleri hackleyebiliriz. Kolaydan zora doğru gidecegiz. **Not: Sanal makinelere saldırmak için başlangıç seviyesi Linux bilginiz olmalıdır.**

### Tryhackme

![](https://cdn-images-1.medium.com/max/800/0*lBoFtnWKoQxBHopT.png)

Tryhackme, başlangıç için ideal bir site. Buradaki kolay odalardan başlayarak yetenek setlerinizi geliştirebilirsiniz. Ayrıca buradaki bazı Toolları(araç) odalardan nasıl çalıştığını öğrenebilirsiniz. **Bu toollar arasında Burp Suite kesinlikle olmalıdır. Çünkü bir sonraki site sırf bu tool üzerinedir.**

### Port Swigger-Web Security

![](https://cdn-images-1.medium.com/max/800/0*g9JTcns8dEh9MaH1.png)

Burp Suite, Port Swigger adlı şirketin bir ürünüdür. Bu ürünün ücretli olan Professional ve ücretsiz olan Community versiyonları vardır. Dilediğinizi bilgisayarınıza kurabilirsiniz. Port Swigger, hem kendinizi geliştirmek hemde Burp Suite toolunu daha iyi kullanmanız için [Web Security](https://portswigger.net/web-security) adlı portalını kurdu. Burayı dileğiniz kadar kullanabilirsiniz.

### Hacker 101-CTF

Hackerone adlı Güvenlik Şirketinin hazırladığı bu site sizleri diğer odalara göre biraz daha zorluyacak.  
 [CTF 101](https://ctf.hacker101.com/) Hackerone destekli olduğu için avantaj olarak bir süre sonra Hackerone üzerinden sizlere private bounty programları getirebilir.

### Level 3: Son olarak Tavsiyelerim

![](https://cdn-images-1.medium.com/max/800/0*CBi-sM9Tti4eCw7d.gif)

Sonraki adım olarak bol bol raporlar okumak veya blog yazıları okuyup yazabilirsiniz. Peki blog yazıları veya raporları okumak için nereleri ziyaret etmeliyiz?

Twitter ve Medium bunun için en güzel yerler. Twitterda güvenlik araştırıcılarını veya bazı blog sayfalarını takip edebilirsiniz. Bu konuda [TR Bug Hunterı](https://twitter.com/trbughunters) takip edebilirsiniz. Medium içinse [Bug Bounty WriteUp](https://medium.com/bugbountywriteup) üzerinden çok güzel yazılar yayınlanıyor. Rapor okumak içinse [Hackeronedaki](https://hackerone.com/hacktivity) bölümüne bakabilirsiniz. Bu yazılık bu kadar.

Stay Safe.

**_KAYNAKLAR VE EKSTRA YAZILAR_**

[WEB HACKING RESOURCE KIT](https://view.highspot.com/viewer/5f3aba1ba4dfa0019a6b49c2)

[BEGINNING RESOURCE KIT](https://view.highspot.com/viewer/5f3ab6e7f7794d5f24a9dd05)

[Resources for Beginner Bug Bounty Hunters](https://github.com/nahamsec/Resources-for-Beginner-Bug-Bounty-Hunters/blob/master/assets/basics.md)

[LiveOverflow’un YouTube Kanalı](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)

[Stök’ün YouTube Kanalı](https://www.youtube.com/channel/UCQN2DsjnYH60SFBIA6IkNwg)

[Hackeone Hacktivitys](https://hackerone.com/hacktivity)

[Utku Şenin Blogu](https://utkusen.com/blog/)

* * *

_Originally published at_ [_https://selcuk.live_](https://selcuk.live/web-guvenligi-yol-haritasi)_._

By [Selcuk TATAR](https://medium.com/@tansionline) on [December 23, 2020](https://medium.com/p/5cde4ef97c60).

[Canonical link](https://medium.com/@tansionline/web-g%C3%BCvenli%C4%9Fi-yol-haritas%C4%B1-5cde4ef97c60)

Exported from [Medium](https://medium.com) on January 18, 2022.