---
title: The 5 Biggest Plane Crashes in The World
date: 2020-04-25
---

### 5\. Air India Flight 182

![](https://cdn-images-1.medium.com/max/800/1*eDqHOqNttoi029E3HdUozg.jpeg)

**Location:** Atlantic Ocean

**Date:** June 23rd, 1985

**Fatalities:** 329

**Survivors:** 0

**Description:** Air India Flight 182 crashed into the Atlantic Ocean off the coast of Ireland after a bomb exploded in the cargo area. When the bomb went off, the plane was at about 31,000ft. This act of terrorism was linked to the Narita Airport bombing which happened earlier that day.

Air India 182" redirects here. For the 2008 Canadian documentary film, see

### 4\. Turkish Airlines Flight 981

![](https://cdn-images-1.medium.com/max/800/1*xGS-v0C-CtURtFKjLwd9zg.jpeg)

Location: Ermenonville, France

Date: March 3rd, 1974

Fatalities: 346

Survivors: 0

Description: Turkish Airlines Flight 981 crashed into the forest after the cargo door blew off mid-flight. Once the door was open, the cabin depressurized, causing the floor gave way, and destroyed all the connecting lines that control the plane. The flight was in the air for only 72 seconds after the cargo door exploded. The crew of the plane was aware that the lock on the cargo door was possibly damaged, however, even knowing this, the engineers did not check the cargo door properly before takeoff. This was considered to be the deadliest plane crash at the time it occurred.

Results/ Effects: In this specific model of plane, the DC-10, the cargo door opens outward which is very dangerous in a high pressure environment like an airplane. After this incident, there was an industry wide chance to the cargo door as well as the locking mechanisms.

About Video

### 3\. Charkhi Dadri mid-air collision

![](https://cdn-images-1.medium.com/max/800/1*if7aO3CqeW0CmfpYbJtdYA.jpeg)

Location: Charkhi Dadri, India

Date: November 12th, 1996

Fatalities: 349

Survivors: 0

Description: The crash that occurred over Charkhi Dadri was the largest mid-air collision in history. It involved two planes, Saudi Arabian Flight 763 and Kazakhstan Airlines Flight 1907. The Kazakhstan flight was cleared to descend to 15,000ft. while the Saudi Arabian flight was ascending below it. However, Kazakhstan Airlines Flight 1907 descended past the cleared level and hit the top of Saudi Arabian Flight 763. This caused the wing of the Saudi Arabian plane to be severely damaged, and sent it spiraling to the ground with Kazakhstan flight following not far behind.

Crash Debris Video

### 2\. Japan Airlines Flight 123

![](https://cdn-images-1.medium.com/max/800/1*wi7OLOHnmiBODSNHGiQuVg.jpeg)

Location: Ueno, Japan

Date: August 12th, 1985

Fatalities: 520

Survivors: 4

Description: Japan Airlines Flight 123 is the largest single craft accident in history. Due to a mechanical failure that was later linked to an improper repair done to the tail of the plane seven years prior, the pilot was left with little to no control of the aircraft. The pilot was able to keep the plane in the air for close to 30 minutes before it finally crashed into Mount Takamagahara.

Results/ Effects: Repairs done to old planes are now much more rigorously controlled and reviewed.

Cockpit Voice Recorder

### 1\. Tenerife Airport Disaster

![](https://cdn-images-1.medium.com/max/800/1*9lOdmUmgFituvvttruW9mg.jpeg)

Location: Tenerife North Airport, Canary Islands

Date: March 27th, 1977

Fatalities: 583

Survivors: 64

Description: The 1977 airplane crash in Tenerife was the deadliest in history. Due to a terrorist attack on Gran Canaria (Las Palmas) International Airport earlier in the day, several large aircrafts were diverted to the Tenerife Airport. When it came time for the planes to continue on their journeys, two large planes, Pan Am 1736 and KLM 4085, collided in a near head-on crash. The accident was said to have been caused by a combination of the unusually crowded airport, heavy fog, and a miscommunication on the part of the KLM pilot. Out of a combined 644 passengers on both planes, 583 people perished in a fuel fire.

Results/ Effects: Conversations with air traffic control have become much more standardized because of the this incident

About Documentary

Thanks For Reading!

My Website

[www.tansi.online](http://www.tansi.online)

By [Selcuk TATAR](https://medium.com/@tansionline) on [April 25, 2020](https://medium.com/p/e4590554a663).

[Canonical link](https://medium.com/@tansionline/the-5-biggest-plane-crashes-in-the-world-e4590554a663)

Exported from [Medium](https://medium.com) on January 18, 2022.