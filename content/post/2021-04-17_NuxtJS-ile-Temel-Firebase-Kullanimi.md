---
title: NuxtJS ile Temel Firebase Kullanımı
date: 2021-04-17
---

Selamlar, bu yazıda elimden geldiğince NuxtJS yazarken kullandığımız temel Firebase kullanımlarını örneklerle anlatacağım.

* * *

### NuxtJS ile Temel Firebase Kullanımı

Selamlar, bu yazıda elimden geldiğince NuxtJS yazarken kullandığımız temel Firebase kullanımlarını örneklerle anlatacağım.

> Not: Bu yazıda sadece Auth ve Firestore servisi anlatılmaktadır.

* * *

### Bölüm 0: Projenin Oluşturulması ve Konfigüre Edilmesi

    yarn create nuxt-app nuxtjs-firebase

Projeyi oluşturduktan sonra gerekli firebase paketlerini projemize indiriyoruz.

    yarn add firebaseyarn add @nuxtjs/firebase

Gerekli paketleri indirdiğimize göre şimdi [Firebase](https://firebase.google.com/) gidebilir ve yeni bir proje oluşturabiliriz.

![](https://cdn-images-1.medium.com/max/1200/1*q75YFhjHZXUMtzlje3UcOw.png)

Firebase Anasayfa

Eğer önceden giriş yaptıysanız sağ üst kısımdaki **_Go to console_** butonuna tıklayın.

![](https://cdn-images-1.medium.com/max/1200/1*RoG35vZuSffF9ri1m0SGjw.png)

Projeler

Karşınıza projelerinizin olduğu kısım gelecek. Buradan **_Add project_** ile yeni bir proje oluşturuyoruz.

![](https://cdn-images-1.medium.com/max/1200/1*dJumgB7CfuNwX9e5x-2m1g.png)

Yeni projemizin ismi

Yeni projemizin ismini giriyoruz ve **_Continue_** butonuna tıklıyoruz. Sonraki sayfadaki **_Enable Google Analytics for this project_** opsiyonunu kapatıyorum. (siz kullanabilirsiniz)

![](https://cdn-images-1.medium.com/max/1200/1*cWpMmqu4dy46T9xbJ8DpKw.png)

Proje yükleniyor

Her şey yüklendikten sonra Firebase Yönetim Paneline ulaşacağız.

![](https://cdn-images-1.medium.com/max/1200/1*S2pxjsudhCSbQycw7xnxZw.png)

Firebase Yönetim Paneli

Buradan **_Get started by adding Firebase to your app_** yazısının altından soldan 3. ikonu (</>) seçiyoruz.

![](https://cdn-images-1.medium.com/max/1200/1*Mr6EFvVczuV6U5nKVCOuMA.png)

Burada web uygulamamızı kaydetmek için bizden isim istiyor.

![](https://cdn-images-1.medium.com/max/1200/1*zm_aBIb4YhMRu-7G9sDosg.png)

Kayıt için gerekli konfigürasyon

Web uygulamamız için gerekli olan Firebase konfigürasyon bize verdi. Bu kodu kopyalayıp projemize dönüyoruz.

![](https://cdn-images-1.medium.com/max/800/1*InGrKKgRt64g-cQ1Vmlgig.png)

Projemizdeki **_nuxt.config.js_** dosyasını açıyoruz.

![](https://cdn-images-1.medium.com/max/800/1*vZIDPsqCfnkCsAzyUJHIHA.png)

Modules içine kopyaladığımız konfigürasyonu yerleştiriyoruz. Services kısmı ise kullanacağımız servisleri ekliyoruz. Bu yazıda sadece Firestore ve Auth servislerini kullanacağız. [Daha fazla servisler icin.](https://firebase.nuxtjs.org/guide/options/#services)

### Bölüm 1: Auth Servisinin Kullanımı

Bu bölümde uygulamamıza kayıt ve giriş işlemleri için Auth servisini kullanacağız. Öncelikle Firebase içinde bazı ayarları yapılandırıyoruz.

![](https://cdn-images-1.medium.com/max/1200/1*g3p3byII8kOka1HmWOj7kQ.png)

Auth servisi

**_Get started_** butonuna tıklayıp ayarlamızı yapıyoruz.

![](https://cdn-images-1.medium.com/max/1200/1*RqmgipkRDGJtcXV4_HAXgA.png)

**_Email/Password_** kısmını etkinleştiriyoruz. Projemize geçebiliriz.

![](https://cdn-images-1.medium.com/max/800/1*1HPSK5ApxkyQwO0HyrLtAw.png)

Proje içeriği

**_Pages_** klasörü içine **_giris-yap.vue, kayit-ol.vue_** adında iki dosya oluşturuyoruz.

Kayıt olmak için bu kodu kullanacağız:

    async kayıtOl() {  try {     await this.$fire.auth.createUserWithEmailAndPassword(      'tatarselcuk27@gmail.com', 'asdasdasd')  } catch (e) {     alert(e)   }}

Ben kendi projemde biraz düzenledim ve [bu hale](https://github.com/tansionline/nuxtjs-firebase/blob/main/pages/kayit-ol.vue) getirdim.

![](https://cdn-images-1.medium.com/max/800/1*5O_SxvUyRFLnpphhvd6ycw.png)

kayit-ol.vue

Bu sayede her kayıt olduğumda kayıt olan kullanıları Firebasede görebiliyorum.

![](https://cdn-images-1.medium.com/max/1200/1*7iwrMXKOxjVyPJDT-IBtaQ.png)

Kayıt olan kullanıcılar

Giriş yapmak içinse bu kodu kullanıyoruz:

    async girisYap() {  try {     await this.$fire.auth.

Ben tekrar biraz değişiklik yaparak [bu hale](https://github.com/tansionline/nuxtjs-firebase/blob/main/pages/giris-yap.vue) getirdim.

![](https://cdn-images-1.medium.com/max/800/1*GsMSOEo1j6pK6q3fny5r9g.png)

giris-yap.vue

### Bölüm 2: Firestore Servisinin Kullanımı

Bu bölümde Firestore servisi kullanarak basit bir mesajımızı firestore collection içine yazdıracağız ve daha sonra bu yazdırdığımız mesajı çağıracağız. Ilk once firestore ayarlarını yapmamız gerekiyor.

![](https://cdn-images-1.medium.com/max/1200/1*avQKwWxXF3eko7phtVG7eQ.png)

Firestore

**_Firestore Database_** bölümünden **_Create database_** seçeneğine tıklıyoruz.

![](https://cdn-images-1.medium.com/max/800/1*rYqBw23RVUAguJ2njmt6ow.png)

Test Mode

Database oluştururken test modu ile oluşturuyorum.

![](https://cdn-images-1.medium.com/max/1200/1*ZXb1unknEsR8gMq3VJw2HQ.png)

Lokasyonu seçtikten sonra **_Start Collection_** seçeneğine basarak collection oluşturuyoruz.

![](https://cdn-images-1.medium.com/max/800/1*ObfHNXl5wEP_tOS9ZLZLew.png)

**_Collection Id_**’ye isim verdikten sonra **field** ekliyoruz.

![](https://cdn-images-1.medium.com/max/800/1*hWxChgVkL-a9hkp1riTiwg.png)

**_Save_** butonuna bastıktan sonra artık kodumuzu yazmaya hazırız.

Firestore collection içine data yazdırmak için bu kodu kullanıyoruz:

    async firestoreYazdir() {  const messageRef = this.$fire.firestore.collection('mesajlar').doc('doc-id')  try {    await messageRef.set({      mesaj: 'Selam 

Biraz kodu değiştirdim ve [bu hale](https://github.com/tansionline/nuxtjs-firebase/blob/main/pages/firestore-sayfasi.vue) getirdim:

![](https://cdn-images-1.medium.com/max/800/1*wxU0_MQTPm6yhuVdlSA3LA.png)

firestore-sayfasi.vue

Firestore collection içine yazdırdığımız mesajı şimdi ekrana yazdıralım.

Bunun için bu kodu kullanıyoruz:

    async firestoreOku() {  const messageRef = this.$fire.firestore.collection(    'mesajlar').doc('doc-id')  try {    const messageDoc = await messageRef.get()    alert(messageDoc.data().mesaj)  } catch (e) {  alert(e)  return  }}

Bende kendi projemde [şu şekilde](https://github.com/tansionline/nuxtjs-firebase/blob/main/pages/firestore-sayfasi.vue) kullandım:

![](https://cdn-images-1.medium.com/max/800/1*G7n_BG16pGmHdHC5Xvuzxg.png)

firestore-sayfasi.vue

* * *

Elimden geldiğince Firebase ve NuxtJS ile ilgili bildiklerimi aktardım. Bütün kaynak koda buradan ulaşabilirsiniz:

[https://github.com/tansionline/nuxtjs-firebase](https://github.com/tansionline/nuxtjs-firebase)

Eğer yanlışım varsa beni [Twitter](https://twitter.com/tansionline) üzerinden veya yorumlardan uyarabilirsiniz.

* * *

### Kaynaklar

[https://www.w3schools.com](https://www.w3schools.com)

[https://firebase.nuxtjs.org/](https://firebase.nuxtjs.org/)

[https://nuxt-fire-demo.herokuapp.com/](https://nuxt-fire-demo.herokuapp.com/)

By [Selcuk TATAR](https://medium.com/@tansionline) on [April 17, 2021](https://medium.com/p/569d612eacf8).

[Canonical link](https://medium.com/@tansionline/nuxtjs-ile-temel-firebase-kullan%C4%B1m%C4%B1-569d612eacf8)

Exported from [Medium](https://medium.com) on January 18, 2022.